#!/usr/bin/env python

import scapy.all as scapy
import argparse

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", '--target', dest="target", help="IP range you wish to scan - Ex: 10.0.2.1/24")
    options = parser.parse_args()
    if not options.target:
        parser.error("[-] Please specify an IP range, use --help for more info.")

    return options

def scan(ip):
    arp_request = scapy.ARP(pdst=ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    clients_list = []
    for element in answered_list:
        client_dict = {'mac': element[1].hwsrc, 'ip': element[1].psrc}
        clients_list.append(client_dict)

    return clients_list


def print_results(results_list):
    print("IP\t\t\tMAC Address")
    print("-" * 60)
    for client in results_list:
        print('{}\t\t{}'.format(client['ip'], client['mac']))


options = get_args()

scan_result = scan(options.target)
print_results(scan_result)
